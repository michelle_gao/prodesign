package P12_8;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 11/9/13
 * Time: 11:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class Operator {
    public Operator() {

    }

    public static void reStock(Product product,int positionNumber,VendingMachine vendingMachine) {
          Position position= vendingMachine.getPosition(positionNumber);
          position.restock(product);
    }
    public static void stock(VendingMachine vendingMachine) {
        ArrayList<Position> positions = new ArrayList<Position>();
        Product chips = new Product("chips",150);
        Product coke = new Product("coke",200);
        Product cookie = new Product("cookie", 220);
        Product notAvailable = new Product("out of rack", 200);
        for (int i = 0; i < 7; i++) {
            positions.add(new Position(chips));
        }
        for (int i = 0; i < 7; i++) {
            positions.add(new Position(coke));
        }
        for (int i = 0; i < 3; i++) {
            positions.add(new Position(cookie));
        }
        for (int i = 0; i < 3; i++) {
            Position notAvail = new Position(notAvailable);
            notAvail.setAvailable(false);
            positions.add(notAvail);

        }
        vendingMachine.setPositions(positions);
    }
}
