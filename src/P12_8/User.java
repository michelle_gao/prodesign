package P12_8;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 11/10/13
 * Time: 12:15 AM
 * To change this template use File | Settings | File Templates.
 */
public class User {


    public User() {

    }
    public static Position choosePosition(VendingMachine vendingMachine) {
        System.out.println("Please enter the product position number from 1 to 20: ");
        Scanner sc = new Scanner(System.in);
        return vendingMachine.getPosition(sc.nextInt());
    }


    public static int insertCoin(VendingMachine vendingMachine) {
        System.out.println("Please enter the coin cents: ");
        Scanner sc = new Scanner(System.in);
        int coin = sc.nextInt();
        vendingMachine.addMoney(coin);
        return coin;
    }
}
