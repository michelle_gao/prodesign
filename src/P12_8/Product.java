package P12_8;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 11/9/13
 * Time: 10:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class Product {
    private int mCost;
    private String mDescription;
    public Product(String description,int cost) {
        mDescription=description;
        mCost = cost;

    }
    public int getCost() {
        return mCost;
    }

    public String getDescription() {
        return mDescription;
    }
}
