package P12_8;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 11/10/13
 * Time: 9:02 AM
 * P12.8 write a program that simulates a vending machine.Products can be purchased by inserting coins with a value at least
 * equal to the cost of the product. A user selects a product from a list of available products, add coins, and either
 * gets the product or gets the coins returned. The coins are returned if insufficient money wa supplied or if the product
 * is sold out. The machine does not give change if too much money was added. Products can be restocked and money removed
 * by an operator. Follow the design process that was described in the chapter. Your Solutions should include a class
 * VendingMachine that is not coupled with the Scanner or PrintScream classes.
 */
public class VendingMachineDiver {
    public static void main(String[] args) {
        VendingMachine vendingMachine = new VendingMachine();
        Operator.stock(vendingMachine);
        vendingMachine.productList();
        int paidCoins = User.insertCoin(vendingMachine);
        Position mPosition = User.choosePosition(vendingMachine);
        if(mPosition.getAvailable()) {
            if (VendingMachine.isEnoughMoney(paidCoins, mPosition)) {
                vendingMachine.sell(mPosition);
            }
            else {
                vendingMachine.returnMoney(paidCoins);
            }
        }
        else {
            System.out.println("the product is not available in this place.");
            vendingMachine.returnMoney(paidCoins);
        }


    }
}
