package P12_8;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 11/10/13
 * Time: 12:27 AM
 * To change this template use File | Settings | File Templates.
 */
public class VendingMachine {
    private int mMoney;
    private ArrayList<Position> positions;
    public VendingMachine() {
        mMoney = 0;
    }

    public Position getPosition(int iPosition) {
        return positions.get(iPosition);
    }
    public void setPositions(ArrayList<Position> positions) {
        this.positions = positions;
    }


    public void addMoney(int money) {
        this.mMoney += money;
    }
    public static Boolean isEnoughMoney( int coins, Position position) {
        if ( coins >= position.getProduct().getCost() ) {
            return true;
        }
        else {
                System.out.println("The payment is less than the cost of the item.");
                return false;
        }
    }
    public void sell(Position position){
       position.setAvailable(false);
        System.out.println("Purchase succeeded!");
    }
    public void returnMoney(int paidMoney) {
        mMoney-=paidMoney;
        System.out.println("Your money is returned back.");
    }
    public void productList() {
        Product product;
        for (int i = 0; i < positions.size(); i++) {
            product = positions.get(i).getProduct();
            System.out.println(i + " " + product.getDescription() + " " + product.getCost());
        }
    }
}
