package P12_8;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 11/9/13
 * Time: 11:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class Position {
    private Product product;
    private Boolean available;

    public Position(Product product) {
        this.product=product;
        this.available = true;
    }
    public void restock(Product product) {
        this.product= product;
        this.available = true;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Product getProduct() {
        return product;
    }

}
