package P12_6;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 11/9/13
 * Time: 1:09 PM
 * P12.6 Write a program that teaches arithmetic to a young child.The program tests addition adn subtraction. In level 1, it
 * test only addition of numbers less than 10 whose sum is less than 10. In level 2, it tests addition of arbitrary
 * one-digit numbers. In level 3, it tests subtraction of one-digit numbers with a nonnegative difference.
 * Generate random problems and get the player's input. The player get up to two tries per problem.Advance from one
 * level to the next when the player has achieved a score of five points.
 */
public class ArithmeticDriver {
    public static void main(String[] args) {

        Player student = new Player();
        Game game = new Game(student);
        game.start();
    }
}
