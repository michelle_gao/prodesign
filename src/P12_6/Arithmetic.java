package P12_6;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 11/8/13
 * Time: 11:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class Arithmetic {
    private Problem mProblem;
    public Arithmetic(int level) {
        Number number1;
        Number number2;
        String operator;
        switch (level) {
            case 1:
                number1 = new Number(10);
                number2 = new Number(11 - number1.getNumber());
                operator = "+";
                break;
            case 2:
                number1 = new Number(10);
                number2 = new Number(10);
                operator = "+";
                break;
            case 3:
                number1 = new Number(10);
                number2 = new Number(number1.getNumber() + 1);
                operator = "-";
                break;
            default:
                number1 = new Number(0);
                number2 = new Number(0);
                operator = "+";
                break;
        }
        mProblem = new Problem(number1, number2, operator);
    }
    public String format(){
        return mProblem.format();
    }
    public int getAnswer() {
        return mProblem.getAnswer();
    }
}
