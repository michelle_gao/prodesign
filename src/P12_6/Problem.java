package P12_6;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 11/8/13
 * Time: 9:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class Problem {
    private Number mNumber1;
    private Number mNumber2;
    private String mOperator;

    public Problem(Number number1, Number number2, String operator) {
        mNumber1 = number1;
        mNumber2 = number2;
        mOperator = operator;
    }
    public String format() {
        return mNumber1.getNumber() + " " + mOperator + " " + mNumber2.getNumber() + " =  ? ";
    }
    public int getAnswer() {
        switch (mOperator) {
            case "+":
               return mNumber1.getNumber() + mNumber2.getNumber();
            case "-":
                return mNumber1.getNumber() - mNumber2.getNumber();
            default:
                return 0;
        }
    }
}
