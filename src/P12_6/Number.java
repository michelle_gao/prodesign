package P12_6;


import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 11/8/13
 * Time: 9:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class Number {
    private int number;
    public Number(int number) {
        Random rdm = new Random();
        if (number>0) {
            this.number = rdm.nextInt(number);
        }
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String format() {
       return Integer.toString(number);
    }

}
