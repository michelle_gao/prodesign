package P12_6;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 11/9/13
 * Time: 1:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class Game {
    private Player mStudent;
    Arithmetic mArithmetic;
    public Game(Player student) {
        mStudent = student;
    }
    public void start(){
        while (true) {
            if (mStudent.getLevel() >= 4) {
                System.out.println("Congratulations! You got the highest level and score, the game is over!");
                break;
            }
            mArithmetic = new Arithmetic(mStudent.getLevel());
            System.out.println("The arithmetic problem is: " + mArithmetic.format());
            mStudent.play();
            if ( mStudent.getAnswer().compareTo("quit") == 0) {
                System.out.println("The game is exited!");
                break;
            }
            else {
                if(!testAnswer()){
                    System.out.println("The arithmetic problem is: " + mArithmetic.format());
                    mStudent.secondTrial();
                    if (mStudent.getAnswer().compareTo("quit") == 0) {
                        System.out.println("The game is exited!");
                        break;
                    }
                    else testAnswer();
                }
            }
        }
    }
    public Boolean testAnswer() {
        if (Integer.parseInt(mStudent.getAnswer()) == mArithmetic.getAnswer()) {
            mStudent.setScore(mStudent.getScore()+1);
            if (mStudent.getScore() >= 5) {
                mStudent.setLevel(mStudent.getLevel() + 1);
                mStudent.setScore(0);
            }
            System.out.println("current student's score is:" + mStudent.getScore());
            System.out.println("current student's level is:" + mStudent.getLevel());
            return true;
        }
        else  {
            System.out.println("current student's score is:" + mStudent.getScore());
            System.out.println("current student's level is:" + mStudent.getLevel());
            return false;
        }
    }
}
