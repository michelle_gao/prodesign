package P12_6;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Michelle
 * Date: 11/9/13
 * Time: 1:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class Player {
    private int mScore;
    private int mLevel;
    private String mAnswer;

    public Player() {
        mScore = 0;
        mLevel = 1;
    }

    public int getLevel() {
        return mLevel;
    }

    public void setLevel(int mLevel) {
        this.mLevel = mLevel;
    }

    public int getScore() {

        return mScore;
    }

    public void setScore(int mScore) {
        this.mScore = mScore;
    }

    public String getAnswer() {
        return mAnswer;
    }
    public void play() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter the answer or type \"quit\" to exit: ");
        mAnswer = sc.next();
    }
    public void secondTrial() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Your answer is not right, you  have second chance to play, enter your another answer " +
                "or type \"quit\" to exit: ");
        mAnswer = sc.next();
    }

}
